<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="AGM-88 HARM" FOLDED="false" ID="ID_166737161" CREATED="1557520169043" MODIFIED="1557636570086" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="0.754">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" show_icon_for_attributes="true" show_note_icons="true" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="11" RULE="ON_BRANCH_CREATION"/>
<node TEXT="threats" POSITION="left" ID="ID_184923788" CREATED="1557561443663" MODIFIED="1557636570084" HGAP_QUANTITY="178.99999508261695 pt" VSHIFT_QUANTITY="-202.49999396502983 pt">
<edge COLOR="#007c00"/>
<hook NAME="FreeNode"/>
<node TEXT="fixed" ID="ID_1569317167" CREATED="1557576221572" MODIFIED="1557636552746" HGAP_QUANTITY="27.499999597668662 pt" VSHIFT_QUANTITY="-113.24999662488709 pt">
<hook NAME="FreeNode"/>
<node TEXT="A" ID="ID_1122164880" CREATED="1557578066717" MODIFIED="1557578069096"/>
<node TEXT="S" ID="ID_1225085003" CREATED="1557578071173" MODIFIED="1557578076592"/>
<node TEXT="2" OBJECT="java.lang.Long|2" ID="ID_815441094" CREATED="1557578078182" MODIFIED="1557578093241"/>
<node TEXT="3" OBJECT="java.lang.Long|3" ID="ID_435097630" CREATED="1557578094294" MODIFIED="1557578132585"/>
<node TEXT="5" OBJECT="java.lang.Long|5" ID="ID_569929273" CREATED="1557578180185" MODIFIED="1557578181051"/>
<node TEXT="P" ID="ID_1861985825" CREATED="1557578307061" MODIFIED="1557578308992"/>
<node TEXT="N" ID="ID_1766966536" CREATED="1557578294933" MODIFIED="1557578298160"/>
<node TEXT="H" ID="ID_599232289" CREATED="1557578312558" MODIFIED="1557578317192"/>
</node>
<node TEXT="mobile" ID="ID_731619600" CREATED="1557576260325" MODIFIED="1557578141498">
<node TEXT="A" ID="ID_483773534" CREATED="1557578267756" MODIFIED="1557578269311"/>
<node TEXT="S" ID="ID_801064438" CREATED="1557578121015" MODIFIED="1557578141497"/>
<node TEXT="4" OBJECT="java.lang.Long|4" ID="ID_1673385925" CREATED="1557578155424" MODIFIED="1557578157803"/>
<node TEXT="6" OBJECT="java.lang.Long|6" ID="ID_1377732649" CREATED="1557578186393" MODIFIED="1557578187133"/>
<node TEXT="8" OBJECT="java.lang.Long|8" ID="ID_1056070113" CREATED="1557578216826" MODIFIED="1557578217638"/>
<node TEXT="10" OBJECT="java.lang.Long|10" ID="ID_1153669413" CREATED="1557578101486" MODIFIED="1557578107641"/>
<node TEXT="11" OBJECT="java.lang.Long|11" ID="ID_391481606" CREATED="1557578232875" MODIFIED="1557578234365"/>
<node TEXT="13" OBJECT="java.lang.Long|13" ID="ID_240494588" CREATED="1557578238067" MODIFIED="1557578239869"/>
<node TEXT="15" OBJECT="java.lang.Long|15" ID="ID_390860982" CREATED="1557578248283" MODIFIED="1557578252238"/>
<node TEXT="17" OBJECT="java.lang.Long|17" ID="ID_1185666930" CREATED="1557578256396" MODIFIED="1557578262886"/>
<node TEXT="P" ID="ID_268815008" CREATED="1557578418041" MODIFIED="1557578419740"/>
<node TEXT="M" ID="ID_255693450" CREATED="1557578253380" MODIFIED="1557578255838"/>
<node TEXT="C" ID="ID_35156572" CREATED="1557578378528" MODIFIED="1557578379498"/>
</node>
</node>
<node TEXT="not block 15 &amp; MLU&apos;s" POSITION="right" ID="ID_619678592" CREATED="1557520865545" MODIFIED="1557636470473" HGAP_QUANTITY="81.4999979883433 pt" VSHIFT_QUANTITY="-101.24999698251494 pt">
<edge COLOR="#00ffff"/>
<hook NAME="FreeNode"/>
</node>
<node TEXT="stand-alone weapon" POSITION="right" ID="ID_778826191" CREATED="1557520648265" MODIFIED="1557520665108">
<edge COLOR="#ff0000"/>
<node TEXT="POS&#xa;position known" ID="ID_907038887" CREATED="1557520675874" MODIFIED="1557640320538">
<font BOLD="true"/>
<node ID="ID_1611390701" CREATED="1557522148310" MODIFIED="1557584399898"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0033cc">only for fixed targets</font>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="mostly used in SEAD" ID="ID_358668256" CREATED="1557522148310" MODIFIED="1597994154873">
<font BOLD="true"/>
</node>
<node TEXT="u fire @ area and missile finds radar" ID="ID_1768292495" CREATED="1557524177516" MODIFIED="1557524204815"/>
<node ID="ID_1799085080" CREATED="1557526118952" MODIFIED="1557583201920"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b><i>pinky switch</i></b>&#160;toggles modes, when WPN is SOI
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="set target steerpoint (PPT) on WPN page" ID="ID_537061350" CREATED="1557563690415" MODIFIED="1557563793045">
<node ID="ID_877222918" CREATED="1557564183442" MODIFIED="1557583164592"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      - <b><i>ICP #4</i></b>
    </p>
    <p>
      - <b><i>STP #</i></b>&#160;-&gt; <b><i>Enter</i></b>
    </p>
    <p>
      - select thread type
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="set target steerpoint (PPT) on HSD" ID="ID_984775508" CREATED="1557563707536" MODIFIED="1557563802005">
<node ID="ID_940221023" CREATED="1557564382057" MODIFIED="1557583088429"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      - make <b>HSD</b>&#160;the <b>SOI</b>&#160;with <b><i>DMS down</i></b>
    </p>
    <p>
      - move cursor to PPT
    </p>
    <p>
      - use pinky switch to zoom if required
    </p>
    <p>
      - depress <b><i>TMS up</i></b>
    </p>
    <p>
      - confirm in UFC &amp; WPN page correct <b><i>PPT #</i></b>
    </p>
    <p>
      - select thread type
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
<node TEXT="EOM mode" ID="ID_581449601" CREATED="1557522210785" MODIFIED="1557583227159">
<font BOLD="true"/>
<node TEXT="Equation of motion" ID="ID_216260894" CREATED="1557525948394" MODIFIED="1557525954780"/>
<node TEXT="PPT near target" ID="ID_1982874190" CREATED="1557522281900" MODIFIED="1557522290086"/>
<node TEXT="most accurate" ID="ID_1193930917" CREATED="1557525715851" MODIFIED="1557525723940"/>
<node TEXT="activated 40&#xb0; FOW, 5 nm" ID="ID_1735763245" CREATED="1557525738683" MODIFIED="1557525754293"/>
<node TEXT="location well known" ID="ID_806271638" CREATED="1557525834518" MODIFIED="1557525926123"/>
</node>
<node TEXT="PB mode" ID="ID_821252530" CREATED="1557523016027" MODIFIED="1557583230671">
<font BOLD="true"/>
<node TEXT="Pre-briefed" ID="ID_351018026" CREATED="1557525968051" MODIFIED="1557525977837"/>
<node TEXT="long range delivery" ID="ID_1939479108" CREATED="1557525854087" MODIFIED="1557525868001"/>
<node TEXT="high confidence location" ID="ID_1049260558" CREATED="1557525868696" MODIFIED="1557525877186"/>
<node TEXT="120&#xb0; FOW, 15 nm" ID="ID_1025759703" CREATED="1557525906777" MODIFIED="1557525917292"/>
</node>
<node TEXT="RUK mode" ID="ID_290567281" CREATED="1557523022027" MODIFIED="1557583234255">
<font BOLD="true"/>
<node TEXT="range unknown" ID="ID_1663988605" CREATED="1557525990276" MODIFIED="1557525997302"/>
<node TEXT="self defense mode" ID="ID_617171120" CREATED="1557526010668" MODIFIED="1557526015510"/>
<node TEXT="120&#xb0; FOW, immediately" ID="ID_1817919903" CREATED="1557526017037" MODIFIED="1557526038240"/>
</node>
</node>
<node TEXT="HAS&#xa;HARM as sensor" ID="ID_1877419239" CREATED="1557520689091" MODIFIED="1557640331666">
<font BOLD="true"/>
<node ID="ID_1248556442" CREATED="1557522325398" MODIFIED="1557584422244"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <b><font color="#0033cc">moving targets</font></b>
    </p>
  </body>
</html>
</richcontent>
</node>
<node TEXT="displays only emitting radars" ID="ID_1430871561" CREATED="1557563096070" MODIFIED="1557564666918"/>
<node TEXT="threat position is not necessary" ID="ID_625145315" CREATED="1557563150441" MODIFIED="1557564673646"/>
<node TEXT="HAS mode" ID="ID_1945689710" CREATED="1557564767824" MODIFIED="1557583254704">
<font BOLD="true"/>
<node TEXT="Field of view" ID="ID_445637417" CREATED="1557564773384" MODIFIED="1557564778706"/>
<node TEXT="Search Filter page" ID="ID_354077216" CREATED="1557564787200" MODIFIED="1557564795939"/>
<node TEXT="Handoff" ID="ID_1495133613" CREATED="1557564796681" MODIFIED="1557564809987">
<node ID="ID_1200257530" CREATED="1557564812113" MODIFIED="1557583298164"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      - move cursor to thread
    </p>
    <p>
      - <b><i>TMS up</i></b>
    </p>
    <p>
      - when &quot;RDY&quot; -&gt; <b><i>pickle</i></b>
    </p>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
</node>
<node TEXT="DL (datalink)" ID="ID_1346754883" CREATED="1557520706283" MODIFIED="1557520714949">
<node TEXT="not functional yet" ID="ID_119886206" CREATED="1557522312949" MODIFIED="1557522321407"/>
</node>
<node TEXT="block 42/52+/30/32/25" ID="ID_1137197254" CREATED="1557520828144" MODIFIED="1557520860915"/>
<node TEXT="operational differences" ID="ID_490189178" CREATED="1557521529212" MODIFIED="1557521537916">
<node TEXT="sensor is missile itself" ID="ID_1468331029" CREATED="1557521541970" MODIFIED="1557521580462"/>
<node TEXT="targeting through WPN page" ID="ID_505031938" CREATED="1557521581876" MODIFIED="1557521617912"/>
<node TEXT="threat type have to be pre-selected" ID="ID_1209513598" CREATED="1557521619957" MODIFIED="1557563263004"/>
</node>
<node TEXT="HOTAS control summary" ID="ID_1033682601" CREATED="1557565176526" MODIFIED="1557565188665">
<node TEXT="Cursor enable" ID="ID_1842648922" CREATED="1557565194695" MODIFIED="1557584544370">
<font BOLD="true" ITALIC="true"/>
<node TEXT="toggles between POS &amp; HAS modes" ID="ID_623595420" CREATED="1557565202807" MODIFIED="1557565219834"/>
<node TEXT="WPN page needs to be SOI" ID="ID_237856075" CREATED="1557565220320" MODIFIED="1557565230059"/>
</node>
<node TEXT="Cursors" ID="ID_1547868301" CREATED="1557565231544" MODIFIED="1557584551263">
<font BOLD="true" ITALIC="true"/>
<node TEXT="move captain bars in HAS display" ID="ID_940072268" CREATED="1557565238345" MODIFIED="1557565250324"/>
</node>
<node TEXT="TMS up" ID="ID_1146755653" CREATED="1557565252241" MODIFIED="1557584555575">
<font BOLD="true" ITALIC="true"/>
<node TEXT="Handoff the threat in HAS" ID="ID_1897297592" CREATED="1557565259921" MODIFIED="1557565273037"/>
</node>
<node TEXT="TMS aft" ID="ID_1606716659" CREATED="1557565275106" MODIFIED="1557584559383">
<font BOLD="true" ITALIC="true"/>
<node TEXT="deselects the currently selected thread" ID="ID_312364297" CREATED="1557565280914" MODIFIED="1557565294461"/>
</node>
<node TEXT="TMS right" ID="ID_1701638177" CREATED="1557565296027" MODIFIED="1557584562999">
<font BOLD="true" ITALIC="true"/>
<node TEXT="selects first valid threat" ID="ID_1974843335" CREATED="1557565301475" MODIFIED="1557565315022"/>
<node TEXT="2nd TMS right steps to the next thread" ID="ID_829424630" CREATED="1557565315459" MODIFIED="1557565330599"/>
</node>
<node TEXT="TMS left" ID="ID_715557950" CREATED="1557565333244" MODIFIED="1557584567656">
<font BOLD="true" ITALIC="true"/>
<node TEXT="toggles between threat tables" ID="ID_1760119100" CREATED="1557565337012" MODIFIED="1557565347615"/>
</node>
<node TEXT="pinky switch" ID="ID_1603868920" CREATED="1557565349933" MODIFIED="1557584571544">
<font BOLD="true" ITALIC="true"/>
<node TEXT="POS mode cycles the POS flight profile" ID="ID_1230402034" CREATED="1557565354981" MODIFIED="1557565370104"/>
<node TEXT="HAS mode cycles the FOV" ID="ID_545887396" CREATED="1557565370685" MODIFIED="1557565379968"/>
</node>
</node>
</node>
<node TEXT="- set A-G mode&#xa;- SMS page&#xa;- select &quot;AG88&quot;&#xa;- select &quot;PWR OFF&quot; -&gt; &quot;PWR ON&quot;" POSITION="right" ID="ID_348398896" CREATED="1557566543064" MODIFIED="1557636485244" HGAP_QUANTITY="73.24999823421246 pt" VSHIFT_QUANTITY="148.49999557435527 pt">
<edge COLOR="#7c007c"/>
<hook NAME="FreeNode"/>
</node>
<node TEXT="AN/ASQ-213 HTS&#xa;Harm targeting system" POSITION="left" ID="ID_1692153123" CREATED="1557520720740" MODIFIED="1557640171286">
<edge COLOR="#ff00ff"/>
<font BOLD="true"/>
<node TEXT="HAD&#xa;Harm attack display" ID="ID_554681647" CREATED="1557520754877" MODIFIED="1557640295223">
<font BOLD="true"/>
<node ID="ID_247652106" CREATED="1557522361951" MODIFIED="1557584404202"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <p>
      <font color="#0033cc">moving targets</font>
    </p>
  </body>
</html>
</richcontent>
<font BOLD="true"/>
</node>
<node TEXT="- HTS activated (left HDPT)&#xa;- AGM-88 active on SMS&#xa;- A-G mastermode active" ID="ID_1315802278" CREATED="1557567510907" MODIFIED="1557582700209"/>
<node TEXT="Emitter colors" ID="ID_620717921" CREATED="1557567699580" MODIFIED="1557567710765">
<node ID="ID_745931582" CREATED="1557567719362" MODIFIED="1557583860836"><richcontent TYPE="NODE">

<html>
  <head>
    
  </head>
  <body>
    <table border="0" style="width: 80%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 0; border-right-width: 0; border-bottom-width: 0; border-left-width: 0">
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            yellow
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            emitter <b>active</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            red
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            emitter <b>tracking</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            flashing red
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            emitter <b>launching</b>
          </p>
        </td>
      </tr>
      <tr>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            green
          </p>
        </td>
        <td valign="top" style="width: 50%; border-top-style: solid; border-right-style: solid; border-bottom-style: solid; border-left-style: solid; border-top-width: 1; border-right-width: 1; border-bottom-width: 1; border-left-width: 1">
          <p style="margin-top: 1; margin-right: 1; margin-bottom: 1; margin-left: 1">
            emitter <b>not active</b>
          </p>
        </td>
      </tr>
    </table>
  </body>
</html>
</richcontent>
</node>
</node>
</node>
<node TEXT="block 50/52/40" ID="ID_1314734756" CREATED="1557520781022" MODIFIED="1557520801369"/>
<node TEXT="operational differences" ID="ID_1647083742" CREATED="1557521348379" MODIFIED="1557521515272">
<node TEXT="pod is main sensor" ID="ID_1364646046" CREATED="1557521357076" MODIFIED="1557521365525"/>
<node TEXT="HAD page detects mode of operation of radar" ID="ID_1199201390" CREATED="1557521367340" MODIFIED="1557521401335"/>
<node TEXT="more hit probability" ID="ID_539382351" CREATED="1557521403437" MODIFIED="1557521443136"/>
<node TEXT="pod still usable after missiles gone" ID="ID_761832799" CREATED="1557521444287" MODIFIED="1557521474978"/>
<node TEXT="detects all threats not only limited to threat tables" ID="ID_308070702" CREATED="1557521475952" MODIFIED="1557521494946"/>
</node>
</node>
<node TEXT="if SAM pops up on the fly" POSITION="left" ID="ID_1681764991" CREATED="1557635466794" MODIFIED="1557635503590">
<edge COLOR="#007c7c"/>
<node TEXT="on AG radar move SPI to appropriate distance" ID="ID_187469208" CREATED="1557635637787" MODIFIED="1557635660754"/>
<node TEXT="use HAS or HAD to get distance" ID="ID_45897721" CREATED="1557635508622" MODIFIED="1557635711820"/>
<node TEXT="cursor enable to POS mode" ID="ID_952721875" CREATED="1557636137872" MODIFIED="1557636150237"/>
<node TEXT="distance &gt; 25 nm -&gt; PB" ID="ID_1185525657" CREATED="1557635719225" MODIFIED="1557635738878"/>
<node TEXT="distance &lt; 15 nm -&gt; RUK" ID="ID_1817582659" CREATED="1557635741570" MODIFIED="1557635755927"/>
<node TEXT="pop up 30&#xb0; and fire" ID="ID_72561107" CREATED="1557636161745" MODIFIED="1557636176229"/>
</node>
</node>
</map>
