# Falcon BMS - Learning Aid

This repo is a collection of graphs and other materials to help easen the
steep learning curve to play **Falcon BMS**. At least I hope it will help someone.

## HOWTO open the files

### .mm files

I used **Freeplane** to make the .mm files. It is an open source program you can
get frome here: https://www.freeplane.org/wiki/index.php/Home

It is available for Windows, Mac and Linux

### flashcard files

I used **Anki** and **AnkiDroid** but other apps may also work. You may get it from
here: https://apps.ankiweb.net/

It is available for Windows, Mac, Linux and iPhone/Android

## AGM-65 Maverick

A mindmap which gives a fast overview over some important specifications

## AGM-88 HARM

A mindmap which gives an overview about this system

## Flashcards

To help memorize brevities and instrument names. They are grouped into:
- Abbreviations<sup>[1]</sup> (ca. 122 cards)
- Brevities, frequently used brevities<sup>[1]</sup> (ca. 233 cards)
- Brevities in whole sentences, examples for commonly used phrases (ca. 29
cards)
- Picture description, how to describe what you see on the radar<sup>[1]</sup>
(as directory with image files, ca. 77 cards)
- Formations, common formations with pictures
- Mud, ground units, pictures and data, work in progress
- SAMs, capabilities (max. distance/height/min. distance/min. height), how to
dodge<sup>[2]</sup> (ca. 20 cards), work in progress
- Procedures, common procedures with whole brevity sentences

These are simple text files, each card separated by a newline and the front-
and back-card by ';'.

I use them with **Anki** and **AnkiDroid**, they are amazing open source
programs and are available as desktop and mobile phone apps.

It helped me alot. The feature I like most is that it uses the "Leitner system".

You have to use the desktop version to convert the *txt* files to *apkg* files,
specially if you want to use them with the android app. The android app does
not import text files.

**Anki**: https://apps.ankiweb.net/

**AnkiDroid**: https://docs.ankidroid.org/help.html

- Install **Anki** on your computer
- "Create Deck" and name it, for example 'Brevities 1.0'
- click on your new deck
- if you want to include pictures then you will have to copy the images to the
_collection.media_ folder. The location depends on your OS as explained here:
https://docs.ankiweb.net/files.html#file-locations
- go to "File" -> "Import"
- choose the text file
- "Fields separated by: Semicolon"
- Deck -> "the name of your deck"
- check "Allow HTML in fields" -> "Import"
- export your cards: "File" -> "Export" -> Export Format: "Anki Deck Package
(*.apkg)"
- Include: "Brevities 1.0"
- "Include Media" if there are images inside
- "Export" -> "Save"
- copy or email the file to your phone and import it into AnkiDroid to be
always ready to learn some stuff

To use it on your phone with **AnkiDroid**:
- "File" -> "Export" -> "Export format: *.apkg"

I am providing ready apkg-files in _flashcards/apkg/_

**Footnotes:**

<sup>1</sup> Thanks to the **185th squadron** to provide an excellent document with
brevities, phrases and images
http://www.185th.co.uk/files/SOPs/SOP%206%20-%20185th%20Comms%20Procedures%20and%20Brevity/SOP%206-185th%20Comm1-0.pdf

<sup>2</sup> Compiled from The Falcon Lounge - Threat of the Week: https://www.falcon-lounge.com/falcon-bms-essentials/threats-guide/ and The Vault TeeSquare 2019-10-27

## T16000m-TWCS-profile

This is the profile i use on my HOTAS. It is based on some profile I found on
https://benchmarksims.org and I don't remember which one. I think it was _Logic's setup_
"Yet Another T.16000M FCS HOTAS Review and Setup":
https://www.benchmarksims.org/forum/showthread.php?32152-Yet-Another-T-16000M-FCS-HOTAS-Review-and-Setup&highlight=t.16000m

Load _Target4.34.key_ while you are in Falcon BMS through the configuration screen.

### requirements

You absolutely need some kind of tracking device, I use **EDTracker**
http://www.edtracker.org.uk/ and it cost me only 16 Euros on Ebay to buy the
parts and build it. There are other more expensive devices and there is also
a very cheap IR alternative from _Oakdesign_:
https://www.benchmarksims.org/forum/showthread.php?39988-DIY-wireless-IR-Tracker&highlight=oakdesign

Also you need **Target** which you can get on the Thrustmater website:
https://www.thrustmaster.com/en_US/products/target

Start the .tmc-file with
`C:\Program Files (x86)\Thrustmaster\TARGET\x64\TARGETGUI.exe -r c:\where-your-tmc-file-is-located\TWCS-Throttle-FalconBMS.tmc`

Just a bit of advice that helped me a lot:
* use USB 2.x ports, not any USB 3.x ports
* start **Target** as early as you can after login
* use Windows 7, looks like Windows 10 has some problems with **Target**

# Additional ressources:

Home of **Falcon BMS**: https://www.falcon-bms.com/

**Discord**: https://discord.gg/KQNHQBz
