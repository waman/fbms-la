A-A TACAN;Falcon 1, Yardstick up<br>2, timber Sweet<br>1, dolly/timber up<br>2, dolly/timber sweet/sour (link name)
Alpha Check;Falcon 12, alpha check stearpoint 3<br>Falcon 12, alpha steer 3 is 25 miles, 2 miles west of track
Anchor;Falcon Flight, ANCHOR stearpoint 5 in a left hand holding flightlevel 2 2 0
Blind;Falcon 12 is blind, angels 8<br>Falcon 12 continue, One visual, at your left 8 oclock, angels 7<br>2<br><br>Falcon 12 is blind, angels 8<br>Falcon 12 continue, one is bullseye 2 9 8 for 32, angels 7<br>Falcon 12 is blind, angels 7<br>Falcon 12, go to angels 8, one is at bullseye 2 9 8 for 32, angels 7
Buster/Gate/Saunter;Falcons, buster/gate/saunter (100% engine/AB/best endurance)
Calling Enemy Missile Launches;Falcon 11, SAM launch, west<br>Falcon 11, SAM launch, left, 8 oclock<br>Falcon 11, missile launch, left, 2 oclock
Fence Check;1, fence-in/flight fence-in<br>2, fencing in<br>check lights/radar/jammer ->OFF<br>arm weapons<br>2, fence-in check complete, fuel state xx
Flight Direction;Falcon 11, heading waypoint 2, 3 5 0 knots, 10 degree climb, passing angels 3<br>2, in position<br>Falcon Flight buster, turn left heading stearpoint 2, climb up 10.000 feet<br>1, nose up 10°, 250 kts, to flight altitude angels 22/flight level 2 2 0<br>1, turn right whisky 2<br>2, turn right, heading x x x
Holding Short;Falcon 1, holding short RWY 3 6<br>1, runway-time is 19:34, takeoff-time is 19:35<br>Falcon 1, element departure, Gate, rotate 1 8 0, 10° pitch for Angels 20, Buster at 350, left turn for Stp 2, Rejoin box, 3 miles trail
In and Off Target (A-G);Falcon 12, in hot<br>Falcon 12, off west<br>Falcon 12, off dry, west (dry means ordnance was not released)
Maneuvering the Flight;Falcon 11, turning left to waypoint 3, descending to angels 5<br>2 (Turn begins)<br>Falcons, climb Flight Level 2 0 0, 5° nose up<br>Falcon Flight, climb up to flightlevel 2 0 0
Mud;Falcon 12, mud AAA, right 2 oclock<br>Falcon 12, mud 6, east
Painted;Falcon 12, painted, right, 3 oclock
Posit;Falcon 13, say posit<br>3, bullseye 3 5 0 for 23, angels 9<br>3, 7 oclock, 2 miles, angels 9<br>3, 10 miles from steerpoint 4, on course, angels 9<br>3, 10 miles south steerpoint 4, angels 9
Push Tactical;Falcons push tactical<br>Falcon 12, pushing tactical<br>1, radio check tactical<br>2, loud and clear
QNH;Falcon 1, Standby for QNH<br>2<br>Falcon flight set QNH 1 0 1 0<br>2, QNH 1 0 1 0
RWY Start;1, In position and Ready<br>2, In position and Ready<br>1, start with BUSTER/GATE<br>1, Standby Brakes, run´em up<br>2, Standb. All in green<br><br>1, is rolling<br>2, is rolling<br>1, rotate<br>2, rotate<br>1, airborne<br>2, airborne<br>Falcon 11, heading waypoint 2, 3 5 0 knots, 10 degree climb, passing angels 3<br>2, in position
Radio Control;2, ready for up front control radio<br>Falcon 1 push Victor 2<br>2 pushing victor 2<br>Falcon 1 check Victor 2<br>2, read you 5<br>1 = bad (unreadable)<br>2 = poor (readable now and then)<br>3 = fair (readable, but with difficulty)<br>4 = good (readable)<br>5 = excellent (perfectly readable)
Raygun Calls;Falcon 11, Raygun bullseye 2 5 0, 85 miles, 15 thousand<br>Falcon 12 Buddyspike, Buddyspike, (bull 2 5 0, 85, 15 thousand)<br>Falcon 11, Buddylock<br>Falcon 12, Falcon 11, raygun, bullseye 350 for 25, angels 12<br>Falcon 12, naked
Say Fuel;2, say/check fuel state<br>2, fuel state 7 . 3<br>2, 2 thousand<br>3, 2 thousand five<br>4, same (same can be used if with 100lbs of previous call)
Say Status;Falcon 12, say status<br>2, defensive, mud 5<br>2, engaged defensive, MiG-29 (if #2 does not say defensive it is assumed he is offensive)<br>2, engaged, MiG-29 (the absence of defensive means #2 is offensive)<br>2, supporting, 5 oclock, high
Say Weapons;Falcon 1 flight, say weapons<br>2, 1 heater, Dakota and gun<br>3, 1 radar, 2 heater, 4 clusters and gun<br>4, 2 heater, 1 ARM, 4 clusters and gun<br>(radar refers to AMRAAM)<br>(heater refers to Sidewinder)<br>(only say gun if rounds remaining)
Sorting;Falcon 11 is sorted on the leader. Two, sort on the trailer.<br>2, sorted, trailer<br>Falcon 11 is sorted on the high bandit. Two, sort on the low bandit<br>2, sorted, low bandit
Spike;Falcon 12, spike, MiG-19, right 3 oclock
Taking RWY;Kunsan Tower, Falcon 1, taking active runway 3 6 left/right lane for Element/single/2 ship Departure, Full Gate, 5 sec. Spacing, 2 report when ready for departure
Taxi Shelter;1, taxi shelter<br>2, taxi shelter
Taxi;Falcon 1, taxi RWY 3 6 via taxiway Foxtrot and Papa<br>2, taxi
Tumbleweed;Falcon 12 tumbleweed<br>1, single bandit group, 10 miles, cold
