Abort(Ing)(Ed);Directive/informative to cease action/attack/event/mission
Action;Directive to initiate a briefed attack sequence or manoeuvre
Active;An emitter is radiating
Add ( );Directive call to add a specific (system) to search responsibility
Alarm;Directive/informative indicating the termination of EMCON procedures
Alpha Check;Request for/confirmation of bearing and range to described point
Anchor(ed);Orbit about a specific point
Angels;Altitude in thousands of feet (e.g. angels 23 is 23,000 feet)
Arizona;No ARM ordnance remaining
As Fragged;Unit or element will be performing exactly as stated by the air tasking order
Azimuth;Two or more groups primarily separated in bearing
Bandit;An aircraft identified as hostile. The term does not imply direction or authority to engage
Base (Number);Reference number used to indicate such information as headings, altitude, fuels, etc
Beam(ing);Target stabilized within 70 to 110 degree aspect, generally given with directions: east, west, north, south
Bent;System indicated is inoperative
Bingo;Pre-calculated fuel state which means immediate RTB is required
Bird;Friendly surface-to-air missile (SAM)
Bittersweet;Notification of possible blue-on-blue incident (friendly fire)
Blank;A SEAD aircraft does not detect any emitters of interest
Blind;No visual contact with friendly aircraft you are flying with<br>(opposite of Visual)
Blow Through;Directive/informative call, continue straight ahead at the merge and do not turn with bandits(s)
Bogey;A radar or visual air contact whose identity is unknown
Bogey Dope;Request for target information as briefed/available (BRG/Range)
Box;Groups/contacts/formations in a square or offset square
BRAA;Target Bearing, Range, Altitude, and Aspect, relative to a friendly aircraft
Bracket;Indicates geometry where friendly aircraft will manoeuvre to a position on opposing sides, either laterally or vertically from the target
Break (Direction);Directive to perform an immediate turn in the direction indicated. Assumes a defensive situation
Brevity;Radio frequency is becoming saturated/degraded or jammed and briefer transmissions must follow
Broke Lock;Loss of radar/IR lock-on (advisory)
Buddy Spike;Friendly aircraft air-to-air indication on RWR. To be followed by position/heading/altitude
Bugout (Direction);Separation from that particular engagement/attack/operation, no intent to reengage/return
Bullseye;An established point from which the position of an object can be referenced
Buster;Directive call to fly at max continuous speed (ie. maximum without afterburner)
Cap/Capping (Location);1. Directive call to establish an orbit at a specified location<br>2. An orbit at a specified location
Captured;Aircrew has identified and is able to track a specified A/G target with an on-board sensor
Cease Fire;Do not open fire or discontinue firing, complete intercept if weapons are in flight, continue to track
Champagne;An attack of three distinct groups with two in front and one behind
Check ( ) Left/Right;Turn (#) degrees left or right and maintain new heading
Clean;1. No radar contacts on aircraft of interest<br>2. No visible battle damage<br>3. Aircraft not carrying external stores
Clear;No enemy aircraft are a threat to your rear (your six is clear)
Cleared;Requested action is authorised (no engaged/support roles are assumed)
Cleared Hot;Ordnance release is authorised
Closing;Decreasing in range
Cold;1. Attack geometry will result in a pass or roll out behind the target<br>2. On a leg of the CAP pointed away from the anticipated threats<br>3. Group(s) heading away from friendly aircraft<br>(Opposite of Hot)
Comeoff (Left/Right/Low/High);Directive to manoeuvre to either regain mutual support or to de-conflict flight paths for an exchange of engaged and supporting roles. Implies both Visual and Tally
Commit(ed);Fighter intent to engage/intercept, controller continues to provide information
Cons/Conning;Threat/bogey aircraft leaving contrails
Contact;1. Sensor contact at the stated position<br>2. Acknowledges sighting of a specified reference point
Continue;Continue present manoeuvre, does not imply clearance to engage or expend ordnance
Continue Dry;Ordnance release not authorized
Cover(ing);Directive/Informative to take on supporting role and allow Shooter to engage the threat
Crank (Direction);F-Pole manoeuvre, implies illuminating target at radar gimbal limits
Cutoff;Request for, or directive to, intercept using cutoff geometry
Dakota;Radio Call Indicating aircraft is out of air-to-ground ordinance
Data (Object/Position);Standby for message concerning (object) at stated location
Declare;Inquiry as to the identification of a specified track(s), target(s), or correlated group
Defensive (Spike/Missile/SAM/Mud/AAA);Aircraft is in a defensive position and manoeuvring with reference to the stated threat
De-Louse;Directive to detect and identify unknown aircraft trailing friendly aircraft
Deploy;Directive to manoeuvre to briefed positioning
Divert;Proceed to alternate mission/base
Drag(ing) (Direction);Target stabilized at 0-60 degrees aspect
Drop(ing);1. Directive/informative to stop monitoring a specified emitter/target and resume search responsibilities<br>2. Remove the emitter/target from tactical picture/track stores
Echelon;Groups/contacts/formation with wingman displaced approximately 45 degrees behind leader's 3/9 line
Engaged - can also include threat type and if (Offensive/Neutral/Defensive);Manoeuvring with the intent to kill. Implies visual/radar acquisition of target
Estimate;Provides estimate of the size, range, height, or other parameter of a contact, implies degradation
Extend (Direction);Short term manoeuvre to gain energy, distance, or separation, normally with the intent of re-engaging
Eyeball ( );1. Fighter with primary visual identification responsibility<br>2. EO/IR/NVD acquisition of an aircraft. Normally followed by number of aircraft observed
Faded;Radar contact lost
Fast;Target speed is estimated to be 600 knots/mach 1 or greater
Feet (Wet/Dry);Flying over water/land
Fence (In/Out);Set cockpit switches as appropriate prior to entering/exiting the combat area
Flank(ing);Target with a stable aspect of 120 to 150 degrees
Float;Directive/informative to expand formation laterally within visual limits to maintain a radar contact or prepare for a defensive response
Fox (Number);Launch of air-to-air weapons:<br>One - semi-active radar-guided missile (eg Sparrow)<br>Two - infrared-guided missile (eg Sidewinder)<br>Three - active radar-guided missile (eg AMRAAM)
Friendly;A positively identified friendly contact
Furball;A turning fight involving multiple aircraft with known Bandits and Friendlies mixed
Gate;Directive/informative to fly as quickly as possible, using maximum power (after-burner)
Gimbal (Direction);Radar target is approaching azimuth or elevation limits
Goggle/Degoggle;Directive/informative to put on/take off NVD's
Gorilla;Large force of indeterminate numbers and formation
Grandslam;All hostile aircraft of a designated track (or against which a mission was tasked) are shot down
Green (Direction);Direction determined to be clearest of enemy air-to-air activity
Group;Radar targets within approximately 3 NM of each other
Guns;An air-to-air or air-to-surface gunshot
Hard (Direction);High-G, energy sustaining turn
Head-on;Target with an aspect angle of 160 to 180 degrees
Heads Up;Alert of an activity of interest
Heavy;A group or package known to contain three or more entities
High;Between 25,000 and 40,000 ft MSL
Hit(s);1. (A/A) Momentary radar return(s) in search. (Indicates approximate altitude information from fighter.)<br>2. (A/G) Weapons impact within lethal distance
Holding Hands;Aircraft in visual formation
Hold Fire;An emergency fire control order used to stop firing on a designated target, to include destruction of any missiles in flight (where possible)
Home Plate;Home airfield or carrier
Hook (Left/Right);Directive to perform an in-place 180 degree turn
Hostile;A contact identified as enemy upon which clearance to fire is authorized, in accordance with ROE
Hot;1. Attack geometry will result in roll out in front of the target<br>2. On a leg of the CAP pointing toward the anticipated threats<br>3. Group heading towards friendly aircraft<br>4. Ordnance employment intended<br>(Opposite of Cold)
ID;1. Directive to identify the target<br>2. ID accomplished, followed by type
In (Direction);Informative indicating a turn to a hot aspect relative to a threat/target<br>(Opposite of Out)
Joker;Fuel state above Bingo at which separation/bugout/RTB should begin
Judy;Aircrew has radar/visual contact on the correct target, has taken control of the intercept and only requires situation awareness information. Controller will minimize radio transmissions
Kansas;Radio call indicating aircraft is out of air-to-air ordnance
Kill;1. Clearance to fire<br>2. In training, a fighter call to indicate kill criteria have been fulfilled
Knock it Off;Directive to cease air combat manoeuvres/attacks/activities
Ladder;Three or more groups/contacts, in range
Laser On;Directive to start/acknowledge laser designation
Lead-Trail;Tactical formation of two contacts within a group separated in range or following one another
Line Abreast;Two contacts within a group side-by-side
Lights On/Off;Directive to turn on/off exterior lights
Locked (BRAA/Direction);Final radar lock-on, sort is not assumed
Lost Contact;Radar contact lost
Lost Lock;Loss of radar/IR lock-on (advisory)
Low;Target altitude below 10,000 ft AGL
Magnum;Launch of friendly anti-radiation missile
Maddog;Visual AIM-120 launch
Mapping;Multi-function radar in an A/G mode
Marking;Friendly aircraft leaving contrails
Marshal(ing);Establish(ed) at a specific point
Medium;Target altitude between 10,000 ft AGL and 25,000 ft MSL
Merge(d);1. Information that friendlies and targets have arrived in the same visual arena<br>2. Call indicating radar returns have come together
Monitor;Maintain radar awareness on or assume responsibility for specified group
Mover;A moving ground target
Mud (Type/Direction);Indicates RWR ground threat displayed (followed by type and clock position)
Music (on/off);Electronic radar jamming (on/off). See also Strangle
Nails;RWR indication of AI radar in search. Add clock position/azimuth, if known
Naked;No RWR indications
Nevada;Radio call indicating that the aircraft is out of Maverick missiles
New Picture;Used when tactical picture has changed. Supersedes all previous calls and re-establishes picture for all
No Factor;Not a threat
No Joy;Aircrew does not have visual contact with the target/bandit/landmark<br>(opposite of Tally)
Notch(ing) (Direction);Defensive manoeuvre to place threat radar/missile on the beam
Off (Direction);Informative call indicating attack is terminated and manoeuvring to the indicated direction
Offset (Direction);Informative call indicating manoeuvre in a specified direction with reference to the target
On Station;Informative unit/aircraft has reached assigned station
Opening;Increasing in range
Out (Direction);Informative indicating a turn to a cold aspect relative to the threat<br>(opposite of In)
Outlaw;A suspected hostile aircraft
Package;Collection of groups/contacts/formations
Padlocked;Informative call indicating aircrew cannot take eyes off an aircraft or surface position without risk of losing Tally/Visual
Painted;To be illuminated by a search radar
Picture;Tactical situation update
Pince/Pincer;Threat manoeuvring for a bracket attack
Pitbull;Informative, AIM-120 is at active (self-homing) range
Playtime;Amount of time aircraft can remain on station
Pop;1. Starting climb for air-to-surface attack<br>2. Max performance climb out of low altitude structure
Popeye;Flying in clouds or area of reduced visibility
Popup;Informative call of a contact that has suddenly appeared inside of threat range
Posit;Request for position, response in terms of a landmark, waypoint, or a common reference point (bullseye)
Post Attack (Direction);Directive transmission to indicate desired direction after completion of intercept/engagement
Post Hole;Rapid descending spiral
Press;Directive to continue the attack, mutual support will be maintained by caller
Pump;A briefed manoeuvre to low aspect to stop closure on the threat with the intent to re-engage
Pure;Informative indicating pure pursuit is being used or directive to go pure pursuit
Pushing (Group description);1. Departing designated point<br>2. Informative that said group(s) have turned cold and will continue to be monitored
Raygun (Bullseye and Angels);Request for an ID on an unknown aircraft. An immediate Buddy Spike reply from a friendly (Position/Heading/Altitude) aircraft meeting these parameters should be given to prevent a Bittersweet incident
Reference (Direction);Directive to assume stated heading
Reset;Proceed to a pre-briefed position or area of operation
Resume;Resume last formation/station/mission ordered
Retrograde;Directive to withdraw from present position in response to a threat
Rifle;Air-to-Surface missile launch
Ripple;Two or more munitions released or fired in close succession
Rolex (+/- Time);Timeline adjustment in minutes from planned mission execution time, plus is later, minus is earlier)
RTB;Return to base
Saddled;Informative from wingman/element indicating the return to briefed formation position
SAM (Direction);Visual acquisition of a SAM or SAM launch, should include position (clock)
Sandwiched;A situation where an aircraft/element is positioned between opposing aircraft/elements
Saunter;Fly at best endurance
Scram (Direction);Emergency directive to egress for defensive or survival reasons
Scramble;Takeoff as quickly as possible
Separate;Leave a specific engagement, may or may not re-enter
Shackle;A single crossing of flight paths to adjust/regain formation
Shadow;Follow indicated target
Shift;Directive to shift laser illumination
Shooter;Aircraft designated to employ ordnance
Shotgun;Pre-briefed weapons state at which separation/bugout should begin
Skate;Informative/directive to execute launch and leave tactics
Skip It;Veto of fighter commit, usually followed with further directions
Skosh;Aircraft is out of/or unable to employ active radar missiles
Skunk;A maritime surface contact whose identity is unknown 
Slide;Directive call to continue mission while extending range from target in response to threat
Slow;Target with a ground speed of 300 knots or less
Smash;Directive to turn on/off anti-collision lights
Smoke;Smoke marker used to mark a position
Snap (Direction);An immediate vector to the group described
Sniper;Directive for an aircraft to employ a HARM against a specified threat at the specified location
Snooze;Directive/informative indicating initiation of EMCON procedures
Sort;Directive to assign responsibility within a group, criteria can be met visually, by radar or both
Sorted;Sort responsibility has been met
Sour;1. Equipment indicated is operating inefficiently<br>2. Invalid response to an administrative IFF check<br>(Opposite of Sweet)
Spike;Indication of an aircraft radar lock on RWR. Include bearing/clock position and threat type, if known
Spitter (Direction);An aircraft that has departed from the engagement or departs engaged fighters targeting responsibility
Splash;1. (A/A) Target destroyed<br>2. (A/G) Weapons impact
Split;Informative call that flight member is leaving formation to engage a threat, visual may not be maintained
Spot;Acquisition of laser designation
Stack;Two or more groups/contacts/formations with a high/low altitude separation in relation to each other
Status;Request for tactical situation
Steer;Set magnetic heading indicated
Stern;Request for, or directive to, intercept using stern geometry
Stinger;Within a group, a formation of three or more aircraft with a single aircraft in trail
Stranger;Unidentified traffic that is not associated with the action in progress
Strangle ( );Turn off equipment indicated
Strip;Individual fighter/section is leaving the formation to pursue separate attacks
Stripped;Informative call from wingman/element indicating out of briefed formation/position
Strobe;Radar indications of jamming
Sweet;1. Equipment indicated is operating efficiently<br>2. Valid response to an administrative IFF check<br>(Opposite of Sour)
Switch(ed);Indicates an attacker is changing from one aircraft to another
Tag (System W/Position);Known identification of a specific (system) at the stated location
Tally;Sighting of a target, bandit, bogey, or enemy position<br>(Opposite of No Joy)
Target ( );Directive to assign group responsibility to aircraft in a flight
Targeted ( );Group responsibility has been met
Terminate;1. Stop laser illumination of a target<br>2. Cease local engagement without affecting the overall exercise
Threat (Direction);Untargeted Hostile/Bandit/ Bogey within threat range/aspect of a friendly
Throttles;Reduction in power to decrease IR signature
Tied;In radar contact with friendly aircraft or element
Tiger;Enough fuel and ordnance to accept a commit
Tracking;1. Stabilized gun solution<br>2. Continuous illumination of a target<br>3. Contact heading
Trail(er);The last aircraft within a group(s)
Trashed;Informative call that own missile has been defeated
Trespass (Position);The flight is entering a threat ring of an AA system at the stated location
Tumbleweed;Indicates limited situational awareness, assumes No Joy and Blind. Is a request for picture
Unable;Cannot comply as requested/directed
Very High;Above 40,000 ft MSL
Vic;Three groups/contacts with the single closest in range and two contacts, azimuth split, in trail
Visual;In visual contact with friendly aircraft or ground position<br>(Opposite of Blind)
Wall;Three or more groups or contacts primarily split in azimuth
Warning (Colour);Hostile attack is:<br>Red imminent or in progress<br>Yellow probable<br>White improbable (all clear)
Weapons ( );Free: Fire only at targets not positively identified as friendlyin accordance with current ROE<br>Hold/Safe: Fire only in self defence or in response to a formalorder<br>Tight: Fire only at targets positively identified as HOSTILE inaccordance with current ROE
Weeds;Indicates that aircraft are operating close to the surface
What Luck;Request for results of missions or tasks
What State;Report amount of fuel and missiles remaining. Ammunition and oxygen are reported only when specifically requested or critical<br># Active = number of active radar missiles remaining<br># Radar = number of semi-active radar missiles remaining<br># Heat = number of IR missiles remaining<br># Fuel = pounds of fuel or time remaining
Winchester;No ordnance remaining
Words;Directive regarding further information or directives pertinent to mission
Working;1. A SEAD aircraft is gathering information on a designated emitter. Generally followed by signal type (SAM/AAA/EW), bearing, and range, if able<br>2. Aircraft obtaining ID on a specific aircraft/group necessary for BVR employment
Yardstick;Directive to use A/A TACAN for ranging
