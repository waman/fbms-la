<map version="freeplane 1.6.0">
<!--To view this file, download free mind mapping software Freeplane from http://freeplane.sourceforge.net -->
<node TEXT="Maverick AGM-65" FOLDED="false" ID="ID_975756645" CREATED="1558819957238" MODIFIED="1558820004563" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle">
    <properties edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" fit_to_viewport="false"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24.0 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ICON_SIZE="12.0 pt" COLOR="#000000" STYLE="fork">
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important">
<icon BUILTIN="yes"/>
</stylenode>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10.0 pt" SHAPE_VERTICAL_MARGIN="10.0 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="5" RULE="ON_BRANCH_CREATION"/>
<node TEXT="types" POSITION="right" ID="ID_355899615" CREATED="1558820057128" MODIFIED="1558820089323">
<edge COLOR="#ff0000"/>
<node TEXT="A" ID="ID_1665448208" CREATED="1558820091217" MODIFIED="1558820094563">
<node TEXT="white" ID="ID_225529423" CREATED="1558820130466" MODIFIED="1558820133252"/>
</node>
<node TEXT="B" ID="ID_1125639353" CREATED="1558820095497" MODIFIED="1558820097835">
<node TEXT="white" ID="ID_1654542112" CREATED="1558820135515" MODIFIED="1558820137997"/>
</node>
<node TEXT="D" ID="ID_815564613" CREATED="1558820100313" MODIFIED="1558820101668">
<node TEXT="green" ID="ID_845435961" CREATED="1558820139843" MODIFIED="1558820142493"/>
<node TEXT="range 15nm" ID="ID_1256531490" CREATED="1558820180148" MODIFIED="1558820185430"/>
<node TEXT="armoured vehicles" ID="ID_794205448" CREATED="1558820192589" MODIFIED="1558820200871"/>
<node TEXT="small hard targets" ID="ID_925038183" CREATED="1558820202533" MODIFIED="1558820208047"/>
</node>
<node TEXT="G" ID="ID_87730808" CREATED="1558820102209" MODIFIED="1558820110212">
<node TEXT="green" ID="ID_1133492861" CREATED="1558820144651" MODIFIED="1558820147005"/>
<node TEXT="range 20nm" ID="ID_1678653176" CREATED="1558820244567" MODIFIED="1558820251849"/>
<node TEXT="larger target structures" ID="ID_1566204974" CREATED="1558820285784" MODIFIED="1558820302858"/>
</node>
</node>
<node TEXT="time limitation" POSITION="left" ID="ID_634420706" CREATED="1558820423349" MODIFIED="1558820429575">
<edge COLOR="#00ff00"/>
<node TEXT="max. power on time 60 min." ID="ID_835011136" CREATED="1558820434685" MODIFIED="1558820459552"/>
<node TEXT="missiles need 3 min. spin up" ID="ID_87441103" CREATED="1558820464847" MODIFIED="1558820480217"/>
<node TEXT="mx. full power 30 min." ID="ID_595443695" CREATED="1558820481775" MODIFIED="1558820496250"/>
<node TEXT="autom. POWER ON by CNTL on WPN page when pass N/E/S/W steerpoint" ID="ID_1283970063" CREATED="1558820507792" MODIFIED="1558820555348"/>
</node>
<node TEXT="launch restrictions" POSITION="right" ID="ID_1199114143" CREATED="1558820587539" MODIFIED="1558820594181">
<edge COLOR="#ff00ff"/>
<node TEXT="launch speed: below Mach 1.2" ID="ID_1154491306" CREATED="1558820596827" MODIFIED="1558820788548"/>
<node TEXT="max. gimbal offset angle: max. 10&#xb0; azimuth and 15&#xb0; elevation (keyhole)" ID="ID_1739180358" CREATED="1558820616436" MODIFIED="1558820658358"/>
<node TEXT="max. dive angle: 60&#xb0;" ID="ID_1355110750" CREATED="1558820650389" MODIFIED="1558820674360"/>
<node TEXT="max. bank angle: 30&#xb0;" ID="ID_1630132918" CREATED="1558820675102" MODIFIED="1558820685712"/>
<node TEXT="max roll rate: 30&#xb0;/s" ID="ID_878643772" CREATED="1558820686503" MODIFIED="1558820697481"/>
<node TEXT="max. load factor: 3 G" ID="ID_1679164486" CREATED="1558820698095" MODIFIED="1558820708825"/>
<node TEXT="min. load factor: 0.5 G" ID="ID_1809641495" CREATED="1558820709455" MODIFIED="1558820719722"/>
</node>
<node TEXT="modes" POSITION="left" ID="ID_766074346" CREATED="1558820798643" MODIFIED="1558820801309">
<edge COLOR="#00ffff"/>
<node TEXT="PRE (preplanned delivery)" ID="ID_395982877" CREATED="1558820802723" MODIFIED="1558821028816">
<node TEXT="target selection on FCR or TGP" ID="ID_271644567" CREATED="1558821064812" MODIFIED="1558821152226"/>
<node TEXT="LOS has to be boresighted to LOS of sensors" ID="ID_1048309768" CREATED="1558821077693" MODIFIED="1558821098888"/>
</node>
<node TEXT="VIS (visual mode)" ID="ID_838842267" CREATED="1558820807339" MODIFIED="1558821039126">
<node TEXT="target selection with TD box on HUD as SOI" ID="ID_1965765551" CREATED="1558821109046" MODIFIED="1558821136953"/>
</node>
<node TEXT="BORE (boresight)" ID="ID_381207059" CREATED="1558820811188" MODIFIED="1558821054422">
<node TEXT="firing on target of opportunity" ID="ID_493688661" CREATED="1558821162688" MODIFIED="1558821175458"/>
<node TEXT="target selection by placing target within boresight cross on HUD and using WPN page to refine cursor position" ID="ID_986257370" CREATED="1558821223970" MODIFIED="1558821277191"/>
</node>
<node TEXT="mode change by OSB2 (WPNJ) or CURSOR ENABLE (WPN-SOI)" ID="ID_1873916643" CREATED="1558821359896" MODIFIED="1558821402203"/>
</node>
</node>
</map>
